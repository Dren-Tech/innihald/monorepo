# File Service - Model

## Data modell

```mermaid
classDiagram

class File
File : +String : name
File: +UUID : uuid
File: +Storage : storage
File: +Archive : archive
File: +int : version

class Storage

class Archive

Storage <|-- S3
Storage <|-- GoogleStorage
Storage <|-- AzureStorage
Storage <|-- FileSystem

Archive <|-- AmazonGlacier
Archive <|-- SilentCube
Archive <|-- FileSystem
Archive <|-- Tape
Archive <|-- GoogleStorage
Archive <|-- AzureArchiveStorage

File "*" *-- "1" Storage : Contains
File "*" o-- "1" Archive : Contains
```
